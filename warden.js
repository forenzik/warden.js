/*
Copyright (c) 2014 K.J. Markesbery

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/ 

function Warden(success, fail, timeout){
	var warden = this;
	this.callbackSuccess = success;
	this.callbackFail = fail;

	this.alive = false;
	this.done = false;
	this.success = null;
	this.timeout = timeout;
	this.actions = {}
	this.actionKeys = [];

	// Internals //
	this.ensureAction = function(key){
		if (!this.actions.hasOwnProperty(key)){
			var action = {
				key: key,
				done: false,
				success: false,
				args: []
			};
			this.actions[key] = action;
			this.actionKeys.push(key);
			return action;
		}
	}
	this.checkForComplete = function(){
		console.log('Checking for completion');
		if (!warden.alive){
			return false;
		}
		var actions = warden.actions;
		var done = true;
		var success = true;
		for(var i=0;i<warden.actionKeys.length;i++){
			var keyIndex = warden.actionKeys[i]
			var action = warden.actions[keyIndex];
			if (!action.done){
				done = false;
				break;
			}
			if (!action.success){
				success = false;
			}
		}
		if (done){
			warden.done = true;
			warden.success = success;
			warden.doComplete();
		}
	}
	this.forceComplete = function(){
		console.log('Forcing Complete-- TIMEOUT!');
		if(!warden.done){
			warden.done = true;
			warden.success = false;
			warden.doComplete();
		}
	}
	this.onActionComplete = function(key, success, args){
		console.log('Action Complete:', key);
		var action = warden.actions[key];
		action.args = args;
		action.done = true;
		action.success = success;
		warden.checkForComplete();
	}
	this.doComplete = function(){
		console.log('Totally Complete! Running user callback!');
		if(warden.success){
			this.callbackSuccess(warden.actions);
		} else {
			this.callbackFail(warden.actions);
		}
	}
	this.block = function(){
		while(!warden.done){
			42 * 42;
		}
		return warden.success;
	}

	// API //
	this.ready = function(timeout){
		console.log("Ready for action, making alive!");
		if (timeout != undefined){
			warden.timeout = timeout;
		}
		warden.alive = true;
		warden.checkForComplete();
		setTimeout(warden.forceComplete, warden.timeout);
	}
	this.successFor = function(key){
		console.log("Generating success for " + key);
		// Add the new action
		warden.ensureAction(key);
		// Generate callback
		var cb = function(){
			warden.onActionComplete(key, true, arguments);
		}
		return cb;
	}
	this.failureFor = function(key){
		console.log("Generating fail for " + key);
		// Add the new action
		warden.ensureAction(key);
		// Generate callback
		var cb = function(){
			warden.onActionComplete(key, false, arguments);
		}
		return cb;
	}
}
